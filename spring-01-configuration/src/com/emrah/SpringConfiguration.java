package com.emrah;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.emrah")
public class SpringConfiguration {

}
