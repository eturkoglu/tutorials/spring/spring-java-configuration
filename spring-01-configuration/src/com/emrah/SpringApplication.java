package com.emrah;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class SpringApplication {

	public static void main(String[] args) {
		var context = new AnnotationConfigApplicationContext(SpringConfiguration.class);
		var coach = context.getBean("baseballCoach", Coach.class);
		System.out.println(coach.getDailyWorkout());
		context.close();
	}

}
