package com.emrah;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SpringConfiguration {
	
	@Bean
	public FortuneService fortuneService() {
		return new HappyFortuneService();
	}
	
	@Bean
	public Coach baseballCoach() {
		return new BaseballCoach(fortuneService());
	}

}
