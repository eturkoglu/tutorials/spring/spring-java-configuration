package com.emrah;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class SpringApplication {

	public static void main(String[] args) {
		var context = new AnnotationConfigApplicationContext(SpringConfiguration.class);
		var coach = context.getBean("baseballCoach", BaseballCoach.class);
		System.out.println(coach.getDailyWorkout());
		System.out.println(coach.getTeam());
		System.out.println(coach.getEmail());
		context.close();
	}

}
