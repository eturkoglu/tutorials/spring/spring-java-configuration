package com.emrah;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan("com.emrah")
@PropertySource("classpath:sport.properties")
public class SpringConfiguration {

}
