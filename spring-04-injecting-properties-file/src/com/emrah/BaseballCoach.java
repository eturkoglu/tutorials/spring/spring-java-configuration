package com.emrah;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class BaseballCoach implements Coach {
	
	@Value("${foo.team}")
	private String team;
	
	@Value("${foo.email}")
	private String email;

	@Override
	public String getDailyWorkout() {
		return "Spend 30 minutes on batting practice";
	}

	public String getTeam() {
		return team;
	}

	public String getEmail() {
		return email;
	}

}
